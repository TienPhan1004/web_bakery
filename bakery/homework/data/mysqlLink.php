<?php
$mySqlLinks = [
  "a__Thông tin hãng sữa" => "/homework/mysql/hangsua.php",
  "b__Thông tin khách hàng 1" => "/homework/mysql/khachhang-1.php",
  "c__Thông tin khách hàng 2" => "/homework/mysql/khachhang-2.php",
  "d__Thông tin sữa" => "/homework/mysql/sua.php",
  "e__Thông tin sữa (phân trang)" => "/homework/mysql/sua-phantrang.php",
  "f__Thông tin sữa (list đơn giản)" => "/homework/mysql/sua-list.php",
  "g__Thông tin sữa (list dạng cột)" => "/homework/mysql/sua-list-2.php",
  "h__Thông tin sữa (list dạng cột - link)" => "/homework/mysql/sua-list-3.php",
  "l__Thông tin chi tiết sữa (phân trang)" => "/homework/mysql/sua-list-4.php",
  "k__Tìm kiếm sữa (1 tiêu chí)" => "/homework/mysql/sua-timkiem.php",
  "m__Tìm kiếm sữa (nhiều tiêu chí)" => "/homework/mysql/sua-timkiem-2.php",
];
?>
