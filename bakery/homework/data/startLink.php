<?php
$startLinks = [
  "Bài tập 1: In số chẵn, lẻ trong mảng" => "/pmnm/baitap/start/baitap1.php",
  "Bài tập 2: Bảng cửu chương" => "/pmnm/baitap/start/baitap2.php",
  "Bài tập 3: Kiểm tra nguyên tố, tổng số lẻ các phần tử trong mảng" => "/pmnm/baitap/start/baitap3.php",
  "Bài tập về nhà 1: Số đảo, mảng, tổng phần tử lẻ trong mảng" => "/pmnm/baitap/start/vn-baitap1.php",
  "Bài tập về nhà 2: Ma trận" => "/pmnm/baitap/start/vn-baitap2.php",
];
