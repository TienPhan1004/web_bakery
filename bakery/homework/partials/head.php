<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php print $PAGE_TITLE;?></title>

<!-- <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;0,900;1,400;1,700&display=swap" rel="stylesheet"> -->
<link rel="stylesheet" type="text/css" href="/pmnm/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/pmnm/css/style.css">

<script src="/pmnm/js/jquery.js"></script>
<script src="/pmnm/js/popper.min.js"></script>
<script src="/pmnm/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/7312f418af.js", crossorigin="anonymous"></script>
