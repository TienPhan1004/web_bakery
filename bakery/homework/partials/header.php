<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="/pmnm">Open Source</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/pmnm">Home</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="/pmnm/baitap" id="baitap-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Bài tập
        </a>
        <ul class="dropdown-menu" aria-labelledby="baitap-dropdown">
          <li><a class="dropdown-item" href="/pmnm/baitap/start">Start</a></li>
          <li><a class="dropdown-item" href="/pmnm/baitap/form">Form</a></li>
          <li><a class="dropdown-item" href="/pmnm/baitap/array-string-file">Mảng + Chuỗi + File</a></li>
          <li><a class="dropdown-item" href="/pmnm/baitap/OOP">OOP</a></li>
          <li><a class="dropdown-item" href="/pmnm/baitap/mysql">MySQL</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
