<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div class="main">
    <?php
      $arrStr = $_POST['arrStr'] ?? null;
      $num = $_POST['num'] ?? null;
      $text = null;
      if ($arrStr && $arrStr !== '' && is_numeric($num)) {
        $arr = explode(',', $arrStr);
        $find = [];
        foreach($arr as $i => $v)  {
          if ((int)$v === (int)$num) {
            $find[] = $i;
          }
        }
        if (count($find)) {
          $text = "Tìm thấy số $num tại vị trí " . join(', ', $find) . " của mảng";
        } else {
          $text = "Không tìm thấy số $num trong mảng";
        }
      }
    ?>
    <form action="" method="post">
      <table align="center" bgcolor="pink" class="mx-auto">
        <tr>
          <th align="center" bgcolor="hotpink" colspan="2"><h3 class="text-primary text-center">TÌM KIẾM</h3></th>
        </tr>
        <tr>
          <td>Nhập mảng</td>
          <td style="width: 300px">
            <input type="text" name="arrStr" class="form-control form-control-sm" value="<?php echo $arrStr ?>">
          </td>
        </tr>
        <tr>
          <td>Nhập số cần tìm</td>
          <td>
            <input type="number" name="num" class="form-control form-control-sm" value="<?php echo $num ?>">
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <input type="submit" value="Tìm kiếm" class="btn btn-success btn-sm"/>
          </td>
        </tr>
        <tr>
          <td>Mảng</td>
          <td>
            <input type="text" disabled class="form-control form-control-sm" value="<?php echo $arrStr ?>">
          </td>
        </tr>
        <tr>
          <td>Kết quả tìm kiếm</td>
          <td>
            <input type="text" disabled class="form-control form-control-sm" value="<?php echo $text ?>">
          </td>
        </tr>
      </table>
      <div align="center"  class="text-center mt-2">
        (Các phần tử trong mảng cách nhau bởi dấu ",")
      </div>
    </form>

  </div>

</body>
</html>
  
</html>