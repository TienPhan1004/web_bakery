<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div class="main">
    <?php
      $arrStr = $_POST['arrStr'] ?? null;
      $needNum = $_POST['need-num'] ?? null;
      $replaceNum = $_POST['replace-num'] ?? null;
      $old = null;
      $res = null;

      if ($arrStr && $arrStr !== '' && is_numeric($needNum) && is_numeric($replaceNum)) {
        $arr = explode(',', $arrStr);
        $old = join(' ', $arr);
        foreach($arr as $i => $v)  {
          if ((int)$v === (int)$needNum) {
            $arr[$i] = (int)$replaceNum;
          }
        }
        $res = join(' ', $arr);
      }
    ?>

    <form action="" method="post">
      <table  align="center" bgcolor="pink" class="mx-auto">
        <tr>
          <th  align="center" bgcolor="hotpink" colspan="2"><h3 class="text-primary text-center">THAY THẾ</h3></th>
        </tr>
        <tr>
          <td>Nhập các phần tử</td>
          <td style="width: 300px">
            <input type="text" name="arrStr" class="form-control form-control-sm" value="<?php echo $arrStr  ?>">
          </td>
        </tr>
        <tr>
          <td>Giá trị cần thay thế</td>
          <td>
            <input type="number" name="need-num" class="form-control form-control-sm" value="<?php echo $needNum ?>">
          </td>
        </tr>
        <tr>
          <td>Giá trị thay thế</td>
          <td>
            <input type="number" name="replace-num" class="form-control form-control-sm" value="<?php echo $replaceNum ?>">
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <input type="submit" value="Thay thế" class="btn btn-success btn-sm"/>
          </td>
        </tr>
        <tr>
          <td>Mảng củ</td>
          <td>
            <input type="text" disabled class="form-control form-control-sm" value="<?php echo $old ?>">
          </td>
        </tr>
        <tr>
          <td>Mảng sau khi thay thế</td>
          <td>
            <input type="text" disabled class="form-control form-control-sm" value="<?php echo $res ?>">
          </td>
        </tr>
      </table>
      <div align="center" class="text-center mt-2">
        (<span style="color: red" class="text-danger">Ghi chú:</span> Các phần tử trong mảng cách nhau bởi dấu ",")
      </div>
    </form>
  </div>
</body>
</html>

  
</html>