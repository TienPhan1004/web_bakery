<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SORT</title>
</head>
<body>
<?php
function our_swap(&$a,&$b){
    $temp=$a;
    $a=$b;
    $b=$temp;
}
function sxGiam ($arr)
{
    for($i=0;$i<count($arr)-1;$i++)
        for($j=$i+1;$j<count($arr);$j++)
            if($arr[$i] < $arr[$j]) our_swap($arr[$i],$arr[$j]);
    return $arr;
}
function sxTang ($arr)
{
    for($i=0;$i<count($arr)-1;$i++)
        for($j=$i+1;$j<count($arr);$j++)
            if($arr[$i] > $arr[$j]) our_swap($arr[$i],$arr[$j]);
    return $arr;
}
if (isset($_POST['submit'])){
    $text=$_POST['text'];
    $original=explode(",",$text);
    $result_decre = implode(",",sxGiam($original));
    $result_incre = implode(",",sxTang($original));
}
?>

<form action="" method="post">
<table align="center" bgcolor="pink" >
    <th colspan="3" style="text-transform: uppercase" bgcolor="hotpink">
        SẮP XẾP MẢNG
    </th>
    <tr>
        <td>NHẬP MẢNG</td>
        <td> <input type="text" name="text" value="<?php if (isset($text)) echo $text;?>" placeholder="NHẬP CÁC PHẦN TỬ " size="35"> </td>
        <td style="color: red"><h4>(*)</h4> </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <input type="submit" name="submit" value="SẮP XẾP TĂNG GIẢM">
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="color: red"> <b>SAU KHI SẮP XẾP</b></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>TĂNG DẦN</td>
        <td>
            <input type="text" value="<?php if (isset($result_incre)) echo  $result_incre;?>" size="35">
        </td>
        <td></td>
    </tr>
    <tr>
        <td>GIẢM DẦN</td>
        <td>
            <input type="text" value="<?php if (isset($result_decre)) echo  $result_decre;?>" size="35">
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center">
            <b style="color: red">(*)</b> CÁC SỐ CÁCH NHAU BÀNG DẤU PHẨY "<b>,</b>"
        </td>
    </tr>
</table>
</form>
</body>
</html>