<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div class="main">
    <?php
      $n = $_POST['n'] ?? null;
      $arr = [];
      $min = null;
      $max = null;
      $sum = null;
      if (is_numeric($n)) {
        $arr = craeteArrayRand((int)$n);
        $min = min($arr);
        $max = max($arr);
        $sum = array_sum($arr);
      }
    
      function craeteArrayRand($n) {
        $a = [];
        for ($i = 0; $i < $n; $i++) {
          $a[] = rand(0, 20);
        }
        return $a;
      }
    ?>
    <form action="" method="post">
      <table align="center" bgcolor="pink" class="mx-auto">
        <tr>
          <th align="center" bgcolor="hotpink" colspan="2"><h4 class="text-primary text-center">PHÁT SINH MẢNG VÀ TÍNH TOÁN</h4></th>
        </tr>
        <tr>
          <td>Nhập số phần tử</td>
          <td style="width: 300px">
            <input type="number" name="n" class="form-control form-control-sm" value="<?php echo $n ?>">
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <input type="submit" value="Phát sinh và tính toán" class="btn btn-success btn-sm"/>
          </td>
        </tr>
        <tr>
          <td>Mảng</td>
          <td>
            <textarea disabled class="form-control form-control-sm"><?php echo join(' ', $arr) ?></textarea>
          </td>
        </tr>
        <tr>
          <td>GTLN (MAX) trong mảng</td>
          <td>
            <input type="text" disabled class="form-control form-control-sm" value="<?php echo $max ?>">
          </td>
        </tr>
        <tr>
          <td>GTNN (MIN) trong mảng</td>
          <td>
            <input type="text" disabled class="form-control form-control-sm" value="<?php echo $min ?>">
          </td>
        </tr>
        <tr>
          <td>Tổng mảng</td>
          <td>
            <input type="text" disabled class="form-control form-control-sm" value="<?php echo $sum ?>">
          </td>
        </tr>
      </table>
      <div align="center" class="text-center mt-2">
        (<span class="text-danger">Ghi chú: </span> Các phần tử trong mảng có giá trị từ 0 đến 20)
      </div>
    </form>

  </div>
</body>
</html>

</body>
</html>