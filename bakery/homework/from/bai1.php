<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
<body>
 <div class="main">
   <?php
     $err = null;
     $chieudai = $_POST['chieudai'] ?? 0;
     $chieurong = $_POST['chieurong'] ?? 0;

     if ($chieudai !== 0 && $chieurong !== 0) {
       if (is_numeric($chieudai) && is_numeric($chieurong)) {
         $dientich = $chieudai * $chieurong;
       } else {
         $err = 'Hãy nhập đầy đử chiều dài và chiều rộng';
         $dientich = 0;
       }
     } else {
       $dientich = 0;
     }
   ?>

   <em class="text-danger"><?php echo $err ?></em>
   <form  action="" method="post">
     <table align ="center" class="mx-auto" bgcolor="pink">
       <tr>
         <th bgcolor="hotpink" colspan="2" align="center"><h2 class="text-primary">Diện tích hình chữ nhật</h2></th>
       </tr>
       <tr>
         <td>Chiều dài</td>
         <td><input type="number" name="chieudai" value="<?php echo $chieudai ?>"/></td>
       </tr>
       <tr>
         <td>Chiều rộng</td>
         <td><input type="number" name="chieurong" value="<?php echo $chieurong ?>"/></td>
       </tr>
       <tr>
         <td>Diện tích</td>
         <td><input type="text" name="dientich" disabled="disabled" value="<?php echo $dientich ?> "/></td>
       </tr>
       <tr>
         <td align=" center" colspan="2" class="text-center pt-2">
           <input type="submit" value="Tính" class="btn btn-success btn-sm"/>
         </td>
       </tr>
     </table>
   </form>
   
 </div>
 
</body>
</html>

