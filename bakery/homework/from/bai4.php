<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
    <?php
      $toan = $_POST['toan'] ?? null;
      $ly = $_POST['ly'] ?? null;
      $hoa = $_POST['hoa'] ?? null;
      $diemChuan = $_POST['chuan'] ?? null;
      $tong = null;
      $ketQua = null;

      if ($toan !== null && $ly !== null && $hoa !== null && $diemChuan) {
        $tong = $toan + $ly + $hoa;
        if ($toan > 0 && $ly > 0 && $hoa > 0 && $tong >= $diemChuan) {
          $ketQua = 'Đậu';
        } else {
          $ketQua = 'Rớt';
        }

      }
    ?>

    <div>
      
      <form action="" method="post">
        <table  align="center" bgcolor="hotpink" class="mx-auto">
          <tr>
            <th bgcolor="pink" colspan="2" >
            <h3 class="text-center text-primary">KẾT QUẢ THI ĐẠI HỌC</h3>
            </th>
          </tr>
          <tr>
            <td>Toán:</td>
            <td><input type="number" name="toan" step="0.0001" min="0" max="10"  value="<?php echo $toan ?>"></td>
          </tr>
          <tr>
            <td>Lý:</td>
            <td><input type="number" name="ly" step="0.0001" min="0" max="10" value="<?php echo $ly ?>"></td>
          </tr>
          <tr>
            <td>Hóa:</td>
            <td><input type="number" name="hoa" step="0.0001" min="0" max="10"  value="<?php echo $hoa ?>"></td>
          </tr>
          <tr>
            <td>Điểm chuẩn:</td>
            <td><input type="number" name="chuan" step="0.0001" min="0"  value="<?php echo $diemChuan ?>"></td>
          </tr>
          <tr>
            <td>Tổng điểm:</td>
            <td><input disabled type="text" value="<?php echo $tong ?>"></td>
          </tr>
          <tr>
            <td>Kết quả thi:</td>
            <td><input disabled type="text" value="<?php echo $ketQua ?>"></td>
          </tr>
          <tr align="center">
            <td><input type="submit" value="Tính" class="btn btn-sm btn-success"></td>
          </tr>
        </table>
       
      </form>
    </div>
  
  </div>

</body>
</html>
