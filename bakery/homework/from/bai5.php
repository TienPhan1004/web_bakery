<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
    <?php
      $batDau = $_POST['batdau'] ?? null;
      $ketThuc = $_POST['ketthuc'] ?? null;
      $tien = null;
      $err = null;

      if ($batDau && $ketThuc) {
        if ($ketThuc > $batDau) {
          if ($ketThuc < 17) {
            $tien = ($ketThuc - $batDau) * 20000;
          } else if ($batDau > 17) {
            $tien = ($ketThuc - $batDau) * 45000;
          } else {
            $tien = (17 - $batDau) * 20000 + ($ketThuc - 17) * 45000;
          }
        } else {
          $err = 'Giờ kết thúc phải lớn hơn giờ bắt đầu';
        }
      }
    ?>

    <div>
      
      <em class="text-danger"><?php echo $err ?></em>
      <form action="" method="post">
        <table align="center" bgcolor="pink" class="mx-auto">
          <tr >
            <th bgcolor="hotpink" align="center" colspan="3">
            <h3 class="text-center text-primary">TÍNH TIỀN KARAOKE</h3>
            </th>
          </tr>
          <tr>
            <td>Giờ bắt đầu:</td>
            <td><input type="number" style="width: 100%" name="batdau" min="10" max="24" step="0.001" value="<?php echo $batDau ?>"></td>
            <td>(h)</td>
          </tr>
          <tr>
            <td>Giờ kết thúc</td>
            <td><input type="number" style="width: 100%" min="10" max="24" step="0.001" name="ketthuc" value="<?php echo $ketThuc ?>"></td>
            <td>(h)</td>
          </tr>
          <tr>
            <td>Tiền thanh toán:</td>
            <td><input disabled type="text" value="<?php echo $tien ?>"></td>
            <td>(VNĐ)</td>
          </tr>
          <tr>
          <td align="center" colspan="3"class="mt-2 text-center">
          <input type="submit" value="Tính tiền" class="btn btn-sm btn-success">
          </td>
          </tr>
          
        </table>
        
      </form>
    </div>
   
  </div>
  
</body>
</html>

  