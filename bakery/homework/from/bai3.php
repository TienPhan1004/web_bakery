<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  
<div bgcolor="red" align="center" class="main">
    <?php
      $err = null;
      $chuHo = $_POST['chuho'] ?? null;
      $soCu = $_POST['socu'] ?? null;
      $soMoi = $_POST['somoi'] ?? null;
      $donGia = $_POST['gia'] ?? null;
      $tien = null;

      if ($chuHo && $soCu && $soMoi && $donGia && $soMoi >= $soCu) {
        $tien = ($soMoi - $soCu) * $donGia;
      } else {
        $err = 'Vui lòng nhập đầy đủ và chính xác thông tin';
      }
    ?>

    <div>
      
      <form action="" method="post">
        <table bgcolor="pink" style="margin: 0 auto">
        <tr>
          <th bgcolor="hotpink" colspan="3" align="center" >
             <h3  class="text-primary text-center">THANH TOÁN TIỀN ĐIỆN</h3>
          </th>
        </tr>
          <tr>
            <td>Tên chủ hộ</td>
            <td><input type="text" name="chuho" value="<?php echo $chuHo ?>"></td>
          </tr>
          <tr>
            <td>Chỉ số củ</td>
            <td><input type="number" name="socu" value="<?php echo $soCu ?>"></td>
            <td>(Kw)</td>
          </tr>
          <tr>
            <td>Chỉ số mới</td>
            <td><input type="number" name="somoi" value="<?php echo $soMoi ?>"></td>
            <td>(Kw)</td>
          </tr>
          <tr>
            <td>Đơn giá</td>
            <td><input type="number" name="gia" value="<?php echo $donGia ? $donGia : 20000 ?>"></td>
            <td>(VNĐ)</td>
          </tr>
          <tr>
            <td>Số tiền thanh toán</td>
            <td><input disabled type="text" value="<?php echo $tien ?>"></td>
            <td>(VNĐ)</td>
          </tr>
          <tr  align="center" class="text-center mt-2">
             <td><input type="submit" value="Tính" class="btn btn-sm btn-success"></td>
          </tr>
        </table>
        
      </form>
      <em  class="text-danger"><?php echo $err ?></em>
    </div>
   
  </div>
  
</body>
</html>
  
