<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
    <?php
      define("PI", 3.14);
      $err = null;
      $bankinh = $_POST['bankinh'] ?? 0;

      if ($bankinh !== 0) {
        if (is_numeric($bankinh)) {
          $dientich = PI * $bankinh * $bankinh;
          $chuvi = 2 * PI * $bankinh;
        } else {
          $err = 'Hãy nhập bán kính hình tròn';
          $dientich = 0;
          $chuvi = 0;
        }
      } else {
        $dientich = 0;
        $chuvi = 0;
      }
    ?>

    <em class="text-danger"><?php echo $err ?></em>
    <form action="" method="post">
      <table bgcolor="pink" align="center"class="mx-auto">
        <tr>
          <th bgcolor="hotpink" colspan="2" align="center"><h3 class="text-primary">DIỆN TÍCH VÀ CHU VI HÌNH TRÒN</h3></th>
        </tr>
        <tr>
          <td>Bán kính:</td>
          <td><input type="number" name="bankinh" value="<?php  echo $bankinh ?>"/></td>
        </tr>
        <tr>
          <td>Diện tích:</td>
          <td><input type="text" disabled="disabled" value="<?php  echo $dientich ?>"/></td>
        </tr>
        <tr>
          <td>Chu vi:</td>
          <td><input type="text" disabled="disabled" value="<?php  echo $chuvi ?>"/></td>
        </tr>
        <tr>
          <td align="center" colspan="2" class="text-center pt-2">
            <input type="submit" value="Tính" class="btn btn-success btn-sm"/>
          </td>
        </tr>
      </table>
    </form>
   
  </div>
 
</body>
</html>


  