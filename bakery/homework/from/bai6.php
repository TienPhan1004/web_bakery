<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
    <div class="text-center">
    
      <form action="bai6_r.php" method="post">
        <table align="center" bgcolor="pink" class="mx-auto">
          <tr>
            <td align="center" bgcolor="hotpink" colspan="5">
            <h3 class="text-center text-primary">PHÉP TÍNH TRÊN HAI SỐ</h3>
            </td>
          </tr>
          <tr>
            <td>Chọn phép tính:</td>
            <td>
              <input type="radio" name="pheptinh" id="cong" value="+">
              <label for="cong">Cộng</label>
              <input type="radio" name="pheptinh" id="tru" value="-">
              <label for="tru">Trừ</label>
              <input type="radio" name="pheptinh" id="nhan" value="*">
              <label for="nhan">Nhân</label>
              <input type="radio" name="pheptinh" id="chia" value="/">
              <label for="chia">Chia</label>
            </td>
          </tr>
          <tr>
            <td>Số thứ nhất:</td>
            <td><input type="number" step="0.001" name="so1"></td>
          </tr>
          <tr>
            <td>Số thứ nhì:</td>
            <td><input type="number" step="0.001" name="so2"></td>
          </tr>
          <tr>
          <td colspan="2"align="center" class="mt-2 text-center">
             <input type="submit" value="Tính" class="btn btn-sm btn-success">
          </td>
          </tr>
        </table>
        
      </form>
    </div>
  
  </div>
 
</body>
</html>

 