<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
    <?php
      $so1 = $_POST['so1'] ?? null;
      $so2 = $_POST['so2'] ?? null;
      $phepTinh = $_POST['pheptinh'] ?? null;
      $kq = null;
      $phepTinhStr = null;
      $err = null;

      if (is_numeric($so1) && is_numeric($so2) && $phepTinh) {
        switch($phepTinh) {
          case "+":
            $kq = $so1 + $so2;
            $phepTinhStr = 'Cộng';
            break;
          case "-":
            $kq = $so1 - $so2; 
            $phepTinhStr = 'Trừ'; 
            break;
          case "*":
            $kq = $so1 * $so2; 
            $phepTinhStr = 'Nhân'; 
            break;
          case "/":
            if ($so2 != 0) {
              $kq = $so1 / $so2;
            } else {
              $err = "Lỗi chia cho 0";
            }
            $phepTinhStr = 'Chia';
            break;
          default: $err = "Phép tính không hợp lệ"; break;
        }
      }
    ?>

    <div class="text-center">
      
      <em class="text-danger"><?php echo $err ?></em>
      <table align="center" bgcolor="pink"class="mx-auto">
        <tr>
          <th align="center" bgcolor="hotpink" colspan="5">
          <h3 class="text-center text-primary">PHÉP TÍNH TRÊN HAI SỐ</h3>
          </th>
        </tr>
        <tr>
          <td>Chọn phép tính:</td>
          <td><?php echo $phepTinhStr ?></td>
        </tr>
        <tr>
          <td>Số 1</td>
          <td><input type="number" disabled value="<?php echo $so1 ?>" ></td>
        </tr>
        <tr>
          <td>Số 2</td>
          <td><input type="number" disabled value="<?php echo $so2 ?>"></td>
        </tr>
        <tr>
          <td>Kết quả</td>
          <td><input type="number" disabled value="<?php echo $kq ?>"></td>
        </tr>
      </table>
    </div>
   
  </div>
<div align="center">
  <a href="javascript:window.history.back(-1);" class="btn btn-secondary btn-sm mt-5">Quay lại trang trước</a>
</div>
</body>
</html>
