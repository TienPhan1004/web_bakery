<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
  <?php
    require('connect.php');

    $sql = 'select Ma_hang_sua, Ten_hang_sua, Dia_chi, Dien_thoai, Email from hang_sua';
    $result = mysqli_query($conn, $sql); ?>

    <h3 align="center" class="text-center text-primary">THÔNG TIN HÃNG SỮA</h3>
    <table align="center" bgcolor="pink" class="table table-sm table-bordered" border="1">
      <thead>
        <tr bgcolor="hotpink" class="text-center">
          <th>Mã hãng sửa</th>
          <th>Tên hãng sửa</th>
          <th>Địa chỉ</th>
          <th>Điện thoại</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          if(mysqli_num_rows($result) !== 0) {
            while($rows = mysqli_fetch_array($result)) { ?>
              <tr>
                <td><?php echo $rows['Ma_hang_sua'] ?></td>
                <td><?php echo $rows['Ten_hang_sua'] ?></td>
                <td><?php echo $rows['Dia_chi'] ?></td>
                <td><?php echo $rows['Dien_thoai'] ?></td>
                <td><?php echo $rows['Email'] ?></td>
              </tr>
            <?php }
          }
        ?>
      </tbody>
    </table>

    <?php
      mysqli_free_result($result);
      mysqli_close($conn);
    ?>

</body>
</html>

  
</html>
