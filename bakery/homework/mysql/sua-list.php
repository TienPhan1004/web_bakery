<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
    <h3 class="text-primary text-center">THÔNG TIN CÁC SẢN PHẨM</h3>
    <?php
      require('connect.php');

      $sql = "SELECT ma_sua, ten_sua, trong_luong, don_gia, ten_hang_sua, ten_loai, hinh
              FROM sua s JOIN hang_sua hs on s.ma_hang_sua = hs.ma_hang_sua
                         JOIN loai_sua ls on s.ma_loai_sua = ls.ma_loai_sua";

      $result = mysqli_query($conn, $sql);

      $imagePath = '/homework/images/';
    ?>

    <?php 
      if(mysqli_num_rows($result) !== 0) {
        while($rows = mysqli_fetch_array($result)) { ?>
          <div class="card mb-3 mx-auto" style="max-width: 540px;">
            <div class="row align-items-center no-gutters">
              <div class="col-4">
                <img src="<?php echo $imagePath . $rows['hinh'] ?>" class="card-img" alt="...">
              </div>
              <div class="col-8">
                <div class="card-body">
                  <h5 class="card-title">
                    <strong><?php echo $rows['ten_sua'] ?></strong>
                  </h5>
                  <p class="card-text"><?php echo "Nhà sản xuất: " . $rows['ten_hang_sua'] ?></p>
                  <p class="card-text">
                    <?php echo $rows['ten_loai'] . " - " . $rows['trong_luong'] . " gram - " . number_format($rows['don_gia'], 0, ',', '.') . " VNĐ" ?>
                  </p>
                </div>
              </div>
            </div>
          </div>
        <?php }
      }
    ?>
    
    <?php
      mysqli_free_result($result);
      mysqli_close($conn);
    ?>

</body>
</html>

  
