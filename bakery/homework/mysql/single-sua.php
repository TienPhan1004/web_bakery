<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
    <?php
      require('connect.php');
      $idSua = $_GET['id'] ?? '';
      $sql = "SELECT ma_sua, ten_sua, trong_luong, don_gia, ten_hang_sua, hinh, loi_ich, tp_dinh_duong
              FROM sua s JOIN hang_sua hs on s.ma_hang_sua = hs.ma_hang_sua
              WHERE ma_sua = '$idSua'";

      $result = mysqli_query($conn, $sql);

      $imagePath = '/homework/images/';
    ?>

      <?php 
        if(mysqli_num_rows($result) !== 0) {
          $row = mysqli_fetch_array($result) ?>
          <h3 class="text-primary text-center"><?php echo "{$row['ten_sua']} - {$row['ten_hang_sua']}" ?></h3>
          <div class="d-flex flex-wrap justify-content-center">
            <div class="card mb-3 mx-auto" style="max-width: 800px;">
            <div class="row align-items-center no-gutters">
              <div class="col-4">
                <img src="<?php echo $imagePath . $row['hinh'] ?>" class="card-img" alt="...">
              </div>
              <div class="col-8">
                <div class="card-body">
                  <p>
                    <strong>Thành phần dinh dưỡng</strong>
                    <br>
                    <span><?php echo $row['tp_dinh_duong'] ?></span>
                  </p>
                  <p>
                    <strong>Lợi ích</strong>
                    <br>
                    <span><?php echo $row['loi_ich'] ?></span>
                  </p>
                  <p class="card-text text-right">
                    <strong>Trọng lương: </strong>
                    <?php echo $row['trong_luong'] . " gram" ?>
                    <strong> - Đơn giá</strong>
                    <?php  echo number_format($row['don_gia'], 0, ',', '.') . " VNĐ" ?>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <?php }
      ?>
    </div>
    
    <?php
      mysqli_free_result($result);
      mysqli_close($conn);
    ?>
</body>
</html>
