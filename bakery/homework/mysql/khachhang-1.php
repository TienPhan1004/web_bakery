<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
  <?php
    require('connect.php');

    $sql = 'select ma_khach_hang, ten_khach_hang, phai, dia_chi, dien_thoai from khach_hang';
    $result = mysqli_query($conn, $sql);
  ?>
    
    <h3 align="center" class="text-center text-primary">THÔNG TIN KHÁCH HÀNG</h3>
    <table align="center" bgcolor="pink" class="table table-sm table-bordered table-striped" border="1">
      <thead>
        <tr align="center" bgcolor="hotpink"  class="text-center text-danger">
          <th>Mã KH</th>
          <th>Tên khách hàng</th>
          <th>Giới tính</th>
          <th>Địa chỉ</th>
          <th>Điện thoại</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          if(mysqli_num_rows($result) !== 0) {
            while($rows = mysqli_fetch_array($result)) { ?>
              <tr>
                <td><?php echo $rows['ma_khach_hang'] ?></td>
                <td><?php echo $rows['ten_khach_hang'] ?></td>
                <td class="text-center"><?php echo $rows['phai'] ?></td>
                <td><?php echo $rows['dia_chi'] ?></td>
                <td><?php echo $rows['dien_thoai'] ?></td>
              </tr>
            <?php }
          }
        ?>
      </tbody>
    </table>

    <?php
      mysqli_free_result($result);
      mysqli_close($conn);
    ?>
</body>
</html>

  
