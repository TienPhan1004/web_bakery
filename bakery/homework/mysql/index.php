<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
    <h1>Bài tập MySQL</h1>
    <?php include("../data/mysqlLink.php");?>
    <div class="list-group">
      <?php 
        foreach($mySqlLinks as $name => $link) { ?>
          <a href="<?php echo $link ?>" class="list-group-item list-group-item-action"><?php echo $name ?></a>
        <?php }
      ?>
    </div>
  </div>

  <?php include('../partials/footer.php') ?>
  <?php include('../partials/foot.php'); ?>
</body>
</html>
  
