<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
 
<div class="main">
    <?php
      require('connect.php');

      $rowsPerPage = 2; //số mẩu tin trên mỗi trang, giả sử là 10
      $pageCur = $_GET['page'] ?? 1;
      $offset = ($pageCur - 1) * $rowsPerPage;
      
      $sql = "SELECT ma_sua, ten_sua, trong_luong, don_gia, ten_hang_sua, hinh, loi_ich, tp_dinh_duong
              FROM sua s JOIN hang_sua hs on s.ma_hang_sua = hs.ma_hang_sua
              LIMIT $offset, $rowsPerPage";

      $result = mysqli_query($conn, $sql);
      $imagePath = '/pmnm/images/';
    ?>

    <h3 class="text-center text-primary">THÔNG TIN CHI TIẾT CÁC LOẠI SỮA</h3>
    
    <?php 
      if(mysqli_num_rows($result) !== 0) {
        while($rows = mysqli_fetch_array($result)) { ?>
          <div class="d-flex flex-wrap justify-content-center">
            <div class="card mb-3 mx-auto" style="max-width: 800px;">
              <div class="row align-items-center no-gutters">
                <div class="col-4">
                  <img src="<?php echo $imagePath . $rows['hinh'] ?>" class="card-img" alt="...">
                </div>
                <div class="col-8">
                  <div class="card-body">
                  <h4 class="text-primary text-center"><?php echo "{$rows['ten_sua']} - {$rows['ten_hang_sua']}" ?></h4>
                    <p>
                      <strong>Thành phần dinh dưỡng</strong>
                      <br>
                      <span><?php echo $rows['tp_dinh_duong'] ?></span>
                    </p>
                    <p>
                      <strong>Lợi ích</strong>
                      <br>
                      <span><?php echo $rows['loi_ich'] ?></span>
                    </p>
                    <p class="card-text text-right">
                      <strong>Trọng lương: </strong>
                      <?php echo $rows['trong_luong'] . " gram" ?>
                      <strong> - Đơn giá</strong>
                      <?php  echo number_format($rows['don_gia'], 0, ',', '.') . " VNĐ" ?>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php }
      }
    ?>

    <?php
      $result2 = mysqli_query($conn, 'select count(*) as num_rows from sua');

      $numRows = mysqli_fetch_array($result2)['num_rows'];

      //tổng số trang
      $maxPage = ceil($numRows / $rowsPerPage);
    ?>
    <nav aria-label="Page navigation example">
      <ul class="pagination pagination-sm justify-content-center">
        <?php 
          if ($pageCur > 1) {
            echo "<li class='page-item'>
              <a class='page-link' href='{$_SERVER['PHP_SELF']}'> << </a>
            </li>";

            echo "<li class='page-item'>
              <a class='page-link' href='{$_SERVER['PHP_SELF']}?page=" . ($pageCur - 1) . "'> < </a>
            </li>";
          }

          for ($i = 1 ; $i <= $maxPage ; $i++) {
            if ($i == $pageCur) {
              echo "<li class='page-item active'><a class='page-link' href='{$_SERVER['PHP_SELF']}?page=$i'>$i</a></li>";
            } else {
              echo "<li class='page-item'><a class='page-link' href='{$_SERVER['PHP_SELF']}?page=$i'>$i</a></li>";
            }
          }

          if ($pageCur < $maxPage) {
            echo "<li class='page-item'>
              <a class='page-link' href='{$_SERVER['PHP_SELF']}?page=" . ($pageCur + 1) . "'> > </a>
            </li>";

            echo "<li class='page-item'>
              <a class='page-link' href='{$_SERVER['PHP_SELF']}?page=$maxPage'> >> </a>
            </li>";
          }
        ?>
      </ul>
    </nav>

    <?php
      mysqli_free_result($result);
      mysqli_close($conn);
    ?>

</body>
</html>
