<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
 
<div class="main">
    <?php
      require('connect.php');

      $rowsPerPage = 5; //số mẩu tin trên mỗi trang, giả sử là 10
      $pageCur = $_GET['page'] ?? 1;
      $offset = ($pageCur - 1) * $rowsPerPage;
      
      $sql = "SELECT ma_sua, ten_sua, trong_luong, don_gia, ten_hang_sua, ten_loai
              FROM sua s JOIN hang_sua hs on s.ma_hang_sua = hs.ma_hang_sua
                         JOIN loai_sua ls on s.ma_loai_sua = ls.ma_loai_sua
              LIMIT $offset, $rowsPerPage";

      $result = mysqli_query($conn, $sql);
    ?>

    <h3 align="center" class="text-center text-primary">THÔNG TIN SỮA</h3>
    <table align="center" bgcolor="pink" class="table table-sm table-bordered table-striped">
      <thead>
        <tr align="center" bgcolor="hotpink"class="text-center text-danger">
          <th>STT</th>
          <th>Tên sữa</th>
          <th>Hãng sữa</th>
          <th>Loại sữa</th>
          <th>Trọng lượng</th>
          <th>Đơn giá</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          if(mysqli_num_rows($result) !== 0) {
            $stt = 1;
            while($rows = mysqli_fetch_array($result)) { ?>
              <tr>
                <td class="text-center"><?php echo $stt ?></td>
                <td><?php echo $rows['ten_sua'] ?></td>
                <td class="text-center"><?php echo $rows['ten_hang_sua'] ?></td>
                <td class="text-center"><?php echo $rows['ten_loai'] ?></td>
                <td class="text-center"><?php echo $rows['trong_luong'] . " gram" ?></td>
                <td class="text-right"><?php echo number_format($rows['don_gia'], 0, ',', '.') . " VNĐ" ?></td>
              </tr>
            <?php $stt++;
            }
          }
        ?>
      </tbody>
    </table>

    <?php
      $result2 = mysqli_query($conn, 'select count(*) as num_rows from sua');

      $numRows = mysqli_fetch_array($result2)['num_rows'];

      //tổng số trang
      $maxPage = ceil($numRows / $rowsPerPage);
    ?>
    <nav aria-label="Page navigation example">
      <ul class="pagination pagination-sm justify-content-center">
        <?php 
          if ($pageCur > 1) {
            echo "<li class='page-item'>
              <a class='page-link' href='{$_SERVER['PHP_SELF']}'> << </a>
            </li>";

            echo "<li class='page-item'>
              <a class='page-link' href='{$_SERVER['PHP_SELF']}?page=" . ($pageCur - 1) . "'> < </a>
            </li>";
          }

          for ($i = 1 ; $i <= $maxPage ; $i++) {
            if ($i == $pageCur) {
              echo "<li class='page-item active'><a class='page-link' href='{$_SERVER['PHP_SELF']}?page=$i'>$i</a></li>";
            } else {
              echo "<li class='page-item'><a class='page-link' href='{$_SERVER['PHP_SELF']}?page=$i'>$i</a></li>";
            }
          }

          if ($pageCur < $maxPage) {
            echo "<li class='page-item'>
              <a class='page-link' href='{$_SERVER['PHP_SELF']}?page=" . ($pageCur + 1) . "'> > </a>
            </li>";

            echo "<li class='page-item'>
              <a class='page-link' href='{$_SERVER['PHP_SELF']}?page=$maxPage'> >> </a>
            </li>";
          }
        ?>
      </ul>
    </nav>

    <?php
      mysqli_free_result($result);
      mysqli_close($conn);
    ?>
</body>
</html>
