<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
    <?php
      // 1. Ket noi CSDL
      require('connect.php');

      // 2. Chuan bi cau truy van & 3. Thuc thi cau truy van
      $sql='select Ma_sua,Ten_sua,Trong_luong,Don_gia from sua';
      $result = mysqli_query($conn, $sql);

      // 4.Xu ly du lieu tra ve
      echo "<h3 class='text-center text-primary'>THÔNG TIN SỮA</h3>";
      echo "<table class='table table-sm table-bordered'>";
      echo '<tr class="text-center">
        <th>STT</th>
        <th>Mã sữa</th>
        <th>Tên sữa</th>
        <th>Trọng lượng</th>
        <th>Đơn giá</th>
      </tr>';

      if(mysqli_num_rows($result) != 0) {
        $stt=1;
        while($rows = mysqli_fetch_array($result)) {
          echo "<tr>";
          echo "<td class='text-center'>$stt</td>";
          echo "<td class='text-center'>{$rows['Ma_sua']}</td>";
          echo "<td>{$rows['Ten_sua']}</td>";
          echo "<td class='text-center'>{$rows['Trong_luong']}</td>";
          echo "<td class='text-right'>". number_format($rows['Don_gia'], 0, ',', '.') . " VNĐ</td>";
          echo "</tr>";
          $stt+=1;
        }//while
      }
      echo "</table>";

      // 5. Xóa ket qua khoi vung nho va Dong ket noi
      mysqli_free_result($result);
      mysqli_close($conn);
    ?>
</html>
