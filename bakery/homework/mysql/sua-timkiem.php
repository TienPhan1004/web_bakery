<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="main">
    <?php
      require('connect.php');

      $ten = $_GET['ten'] ?? '';

      if ($ten !== '') {
        $ten = trim($ten);
      }

      $sql = "SELECT ma_sua, ten_sua, trong_luong, don_gia, ten_hang_sua, hinh, loi_ich, tp_dinh_duong 
              FROM sua s JOIN hang_sua hs on s.ma_hang_sua = hs.ma_hang_sua
              WHERE ten_sua like '%$ten%'";

      $result = mysqli_query($conn, $sql);

      $imagePath = '/homework/images/';

      $count = null;
      if ($ten !== '') {
        $count = mysqli_num_rows($result);
      }
    ?>

    <h3 class="text-primary text-center">TÌM KIẾM THÔNG TIN SỮA</h3>
    <form action="" method="get">
      <div class="form-group row align-items-center container justify-content-center mx-auto" style="max-width: 800px;">
        <label for="ten" class="col-sm-2 col-form-label">Tên sữa</label>
        <div class="col-sm-8">
          <input type="text" class="form-control form-control-sm" id="ten" name="ten" value="<?php echo $ten ?>">
        </div>
        <button type="submit" class="btn btn-sm btn-success">Tìm kiếm</button>
      </div>
    </form>

    <div class="text-center">
      <?php if ($count !== null) {
        echo "<strong>Có $count sản phẩm được tìm thấy</strong>";
      } ?>
    </div>

    <div class="d-flex flex-wrap justify-content-center">
      <?php 
        if(mysqli_num_rows($result) !== 0) {
          while($rows = mysqli_fetch_array($result)) { ?>
            <div class="d-flex flex-wrap justify-content-center">
              <div class="card mb-3 mx-auto" style="max-width: 800px;">
                <div class="row align-items-center no-gutters">
                  <div class="col-4">
                    <img src="<?php echo $imagePath . $rows['hinh'] ?>" class="card-img" alt="...">
                  </div>
                  <div class="col-8">
                    <div class="card-body">
                    <h4 class="text-primary text-center"><?php echo "{$rows['ten_sua']} - {$rows['ten_hang_sua']}" ?></h4>
                      <p>
                        <strong>Thành phần dinh dưỡng</strong>
                        <br>
                        <span><?php echo $rows['tp_dinh_duong'] ?></span>
                      </p>
                      <p>
                        <strong>Lợi ích</strong>
                        <br>
                        <span><?php echo $rows['loi_ich'] ?></span>
                      </p>
                      <p class="card-text text-right">
                        <strong>Trọng lương: </strong>
                        <?php echo $rows['trong_luong'] . " gram" ?>
                        <strong> - Đơn giá</strong>
                        <?php  echo number_format($rows['don_gia'], 0, ',', '.') . " VNĐ" ?>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php }
        }
      ?>
    </div>
    
    <?php
      mysqli_free_result($result);
      mysqli_close($conn);
    ?>
</body>
</html>


 