<?php
include '../backends/connection-pdo.php';

if(isset($_POST['data_key'])){
    $key=$_POST['data_key'];
    $sql = "SELECT * FROM sanpham WHERE name like '%$key%'";
    $query  = $pdoconn->query($sql);
    if ($query->num_rows == 0) {
        echo '<div class="section gray center" style="border: 1px solid black; border-radius: 5px;">
                <p class="header">Xin lỗi hiện chưa có sản phẩm nào!</p>
            </div>';
    } 
    else{
        while ($arr_all=$query->fetch_array()){?>
        <div class="col s12 m4">
            <div class="card">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="./images/<?= $arr_all["image"]; ?>" style="height:233px">
                </div>
                <div class="card-content" >
                    <span class="card-title activator grey-text text-darken-4"><a style="font-size:20px; text-transform:capitalize" class="black-text" href=""><?php echo $arr_all['name']; ?></a><i class="material-icons right">more_vert</i></span>
                <div class="card-content" style="height:62px; margin-top: -20px; " >
                    <p><?php echo $arr_all['price']; ?> VNĐ</p>
                    <p><?php echo $arr_all['status']; ?></p>
                </div>
                
                </div>
                <div class="card-reveal">
                <span class="card-title grey-text text-darken-4"><?php echo $arr_all['name']; ?><i class="material-icons right">close</i></span>
                <p><?php echo $arr_all['mota']; ?></p>
                </div>
            </div>
    </div>
        <?php }
    }
}
?>