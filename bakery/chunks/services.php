<section class="fservices">
		<div class="row" style="padding-left: 50px; padding-right: 50px;">
			<div class="col s12 l4 center" style="padding: 50px 50px;">
				<i class="large material-icons" >local_dining</i>
				<h5 class="header" >Menu đa dạng</h5>
			</div>
			<div class="col s12 l4 center" style="padding: 50px 50px;">
				<i class="large material-icons" >local_shipping</i>
				<h5 class="header" >Giao hàng miễn phí</h5>
			</div>
			<div class="col s12 l4 center" style="padding: 50px 50px;">
				<i class="large material-icons" >mood</i>
				<h5 class="header" >Chất lượng tuyệt vời</h5>
			</div>
		</div>
	</section>