<section class="fdes">
		<div class="section white center">
			<h2 class="header" style="padding:20px; padding-bottom: 30px;">TAKE AWAY BAKERY</h2>
		      <div class="row container center">
		        <div class="col center l8 s12">
		        	<p>
					Các sản phẩm bánh kem tại đây được thiết kế đa dạng với đủ các hương vị khác nhau. Đến với Tous les Jours, bạn sẽ không bị gò bó với bất cứ giới hạn nào, những chiếc bánh mẫu được thay thế bằng sự sáng tạo vô bờ của người làm bánh cũng như khách hàng. Bạn có thể yêu cầu những chiếc bánh kem truyền thống ngọt ngào như Choco Fresh Cream Cake hay mới lạ với chiếc bánh kem phủ kín hoa quả như Rainbow Fresh Cake. Giá của những chiếc tại đây có phần cao hơn những quán khác. Nhưng nếu bạn là một người đam mê bánh ngọt thì đừng ngần ngại mà không chọn cho mình một lần tới đây nhé!
					</p>
		        </div>
		        <div class="col center l4 s12">
		        	<img height="300" width="auto" style="object-fit: contain;" src="images/17964.jpg" alt="">
		        </div>
		        
		      </div>
	</section>