<?php

require('backends/connection-pdo.php');

$sql = 'SELECT * FROM loaisp ';

$query  = $pdoconn->query($sql);
$num=$query->num_rows;
?>


<section class="fcategories">

	<div class="container">

		<div class="section white center">
			<h3 class="header">Categories</h3>
		</div>

<?php
if ($num==0) {
	echo '<div class="section gray center" style="border: 1px solid black; border-radius: 5px;">
			<p class="header">Sorry No Categories to Display!</p>
		</div>';
} else { 
?>
<div class="row">
<?php 
	while($arr_all=$query->fetch_array()){

?>
		<div class="col s12 m12 l4" style="padding: 50px 5px;">
				<a href="foods.php?id=<?=$arr_all['id']?>">
				<div class="card">
					
				    <div class="card-image waves-effect waves-block waves-light">
				      <img class="activator" src="images/<?=$arr_all['image']?>" style="height:233px">
				    </div>
				    <div class="card-content">
				      <span class="card-title activator grey-text text-darken-4"><?php echo $arr_all['name']; ?><i class="material-icons right">more_vert</i></span>
				      <div class="card-content" style="height:110px">
					  <p><?php echo $arr_all['mota_ngan']; ?></p>
			        </div>
				    </div>
				    <div class="card-reveal">
				      <span class="card-title grey-text text-darken-4"><?php echo $arr_all['name']; ?><i class="material-icons right">close</i></span>
				      <p><?php echo $arr_all['mota_dai']; ?></p>
					</div>
				</div>
				</a>
		</div>
			
		

<?php 
		}
	}
?>
</div>
	</div>
	
</section>