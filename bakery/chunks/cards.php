<section class="fcards">
		<div class="row" style="padding-left: 50px; padding-right: 50px; height:197px">
			<div class="col s12 m12 l4" style="padding: 50px 5px;">
				<div class="card">
				    <div class="card-image waves-effect waves-block waves-light">
				      <img class="activator" src="images/bonglan.jpg">
				    </div>
				    <div class="card-content">
				      <span class="card-title activator grey-text text-darken-4">BÁNH BÔNG LAN<i class="material-icons right">more_vert</i></span>
				      <div class="card-content">
			          <p>MẸO LÀM BÁNH BÔNG LAN XỐP - MỀM CỰC TỐI THỨ 5 HẢNG TUẦN (MIỄN PHÍ TẠI TAKE AWAY BAKERY)</p>
			        </div>
				    </div>
				    <div class="card-reveal">
				      <span class="card-title grey-text text-darken-4">BÁNH BÔNG LAN<i class="material-icons right">close</i></span>
				      <p>
					  Bánh bông lan là một trong những loại bánh được rất nhiều người yêu thích, từ người lớn đến trẻ em cũng đều rất thích dùng. Bởi hương thơm và độ mềm XỐP của bánh làm cho người ăn có cảm giác thoải mái, thế nhưng khi làm BÁNH BÔNG LAN thì bạn hay mắc phải một vài “lỗi” là bánh không được mềm, xốp và bị khô nên bánh khi làm ra ăn sẽ bị cứng không có độ đàn hồi. TAKE AWAY Bakery sẽ giúp bạn những mẹo làm bánh bông lan được ngon, mềm, xốp hơn TẠI LỚP HỌC MIỄN PHÍ tHỨ 5 HẢNG TUẦN
					  </p>
				    </div>
				  </div>
			</div>
			<div class="col s12 m12 l4" style="padding: 50px 5px;">
				<div class="card">
				    <div class="card-image waves-effect waves-block waves-light">
				      <img class="activator" src="images/free.jpg">
				    </div>
				    <div class="card-content">
				      <span class="card-title activator grey-text text-darken-4">FREE_SHIP<i class="material-icons right">more_vert</i></span>
				      <div class="card-content">
			          <p>CHUNG TAY VÌ SỨC KHỎE CỘNG ĐỒNG  ĐẶT BÁNH ONLINE FREE SHIP NGAY TẠI NHÀ</p>
			        </div>
				    </div>
				    <div class="card-reveal">
				      <span class="card-title grey-text text-darken-4">FREE_SHIP<i class="material-icons right">close</i></span>
				      <p>
					  Tình hình dịch bệnh ngày một căng thẳng hơn, cùng Take away Bakery chung tay phòng chống dịch Covid. Anh Hòa khuyến khích khách hàng đặt bánh online để đảm bảo an toàn cho tất cả mọi người.
                    💖Thấu hiểu tâm lý chung tay chống dịch, hạn chế đến nơi đông người của các quý khách hàng . Và để bạn vẫn có thể thưởng thức được những chiếc bánh kem nhà Anh Hòa Bakery trong thời điểm này, chúng tôi xin gửi đến bạn những ưu đãi tốt nhất về chính sách ship hàng đó là : CHƯƠNG TRÌNH FREESHIP KHI ĐẶT HÀNG ONLINE VỚI BÁN KÍNH 3KM.
                     🛵Quý khách chỉ cần Xem Menu - Chọn món - Đặt hàng là những chiếc bánh kem nhanh chóng giao đến nơi.
					 🌍 Website: https://takeawaybakery.vn/
                     ☎️ Hotline: 0392355239
					</p>
				    </div>
				  </div>
			</div>
			<div class="col s12 m12 l4" style="padding: 50px 5px;">
				<div class="card">
				    <div class="card-image waves-effect waves-block waves-light">
				      <img class="activator" src="images/women.jpg">
				    </div>
				    <div class="card-content">
				      <span class="card-title activator grey-text text-darken-4">HAPPY WOMEN DAY<i class="material-icons right">more_vert</i></span>
				      <div class="card-content">
			          <p>HAPPY WOMEN DAY'S ĐẶT BÁNH ONLINE  TRAO GỬI YÊU THƯƠNG ĐẾN PHÁI NỮ</p>
			        </div>
				    </div>
				    <div class="card-reveal">
				      <span class="card-title grey-text text-darken-4">HAPPY WOMEN DAY<i class="material-icons right">close</i></span>
				      <p>
					  📌 Đã bao lâu rồi bạn chưa nói lời yêu thương với những người bạn rất quan tâm…
                      🌹🌹🌹 Ngày QUỐC TẾ PHỤ NỮ 8-3 sắp đến rồi, một chiếc bánh kem sẽ là món quà vô cùng ý nghĩa dành tặng cho một nửa thế giới yêu thương. Đừng ngần ngại bày tỏ sự quan tâm những người bà, người mẹ, người vợ người yêu và những người phụ nữ đáng quý xung quanh mình bạn nhé! 😱
					  </p>
				    </div>
				  </div>
			</div>
		</div>
		<div class="row center" style="margin-bottom: 50px;">
			<div class="col s12">
				<a href="food-categories.php" class="waves-effect waves-light btn" style="background: #ee6e73 !important;">More Foods &raquo;</a>
			</div>
		</div>
	</section>