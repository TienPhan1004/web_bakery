
<?php

require('backends/connection-pdo.php');


if (isset($_GET['id'])) {
    $id=$_GET['id'];
	$sql = "SELECT * FROM sanpham WHERE id_loai = '$id'";
	
} else {

	$sql = 'SELECT * FROM sanpham';

}

$query  = $pdoconn->query($sql);



?>
<section class="fcategories">

	<div class="container">

		<?php

			if (isset($_SESSION['msg'])) {
				echo '<div class="section pink center" style="margin: 10px; padding: 3px 10px; margin-top: 35px; border: 2px solid black; border-radius: 5px; color: white;">
						<p><b>'.$_SESSION['msg'].'</b></p>
					</div>';

				unset($_SESSION['msg']);
			}
		?>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
		 <script >
            $(document).ready(function(){
              $('#timkiem').keyup(function(){
                var key=$(this).val();
				$.post('./chunks/food_timkiem.php',{data_key:key},function(data){
                   $('.noidung').html(data);
				});
			  });
			});
		 </script>
		<div class="section white center">
			<h3 class="header">Các sản phậm hiện tại của shop</h3>
			<p style="font-size: 25px;">
			Tìm kiếm 
			<input type="text" name="key" id="timkiem" class="validate" style="border: 3px solid #ee6e73; border-radius: 12px; width: 370px; padding-left: 20px;">
			</p>
		</div>
		<div class="row noidung">
		<?php
			if ($query->num_rows == 0) {
				echo '<div class="section gray center" style="border: 1px solid black; border-radius: 5px;">
						<p class="header">Xin lỗi hiện chưa có sản phẩm nào!</p>
					</div>';
			} 
			else{
				while ($arr_all=$query->fetch_array()){?>
                <div class="col s12 m4">
					<div class="card">
						<div class="card-image waves-effect waves-block waves-light">
							<img class="activator" src="./images/<?= $arr_all["image"]; ?>" style="height:233px">
						</div>
						<div class="card-content" >
							<span class="card-title activator grey-text text-darken-4"><a style="font-size:20px; text-transform:capitalize" class="black-text" href=""><?php echo $arr_all['name']; ?></a><i class="material-icons right">more_vert</i></span>
						<div class="card-content" style="height:62px; margin-top: -20px; " >
							<p><?php echo $arr_all['price']; ?> VNĐ</p>
							<p><?php echo $arr_all['status']; ?></p>
						</div>
						
						</div>
						<div class="card-reveal">
						<span class="card-title grey-text text-darken-4"><?php echo $arr_all['name']; ?><i class="material-icons right">close</i></span>
						<p><?php echo $arr_all['mota']; ?></p>
						</div>
					</div>
			</div>
				<?php }
			}
		?>
		</div>
	</div>
	
</section>