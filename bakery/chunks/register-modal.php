<section class="registermodal">
<form action="./backends/register.php" method="POST">
	<div id="modal2" class="modal">
	
	    <div class="modal-content center">
	      <h4>Take away Bakery Here!</h4>

	      <h5><small class="center" id="reg_error" style="color: red;"></small></h5>
	         

	      	<div class="row">

	      	<div class="input-field col s12">
	          <input required onkeypress="return isAlphaNumSpace(event);"name="fullname" id="name_reg" type="text" class="validate">
	          <label for="name_reg">Full Name</label>
	          
	        </div>

	        <div class="input-field col s12">
	          <input required onkeypress="return isEmail(event);"name="email" id="email_reg" type="email" class="validate">
	          <label for="email_reg">Email</label>
	        </div>
	        <div class="input-field col s12">
	          <input required  name="sdt" id="sdt_reg" type="text" class="validate">
	          <label for="sdt_reg">SĐT</label>
	        </div>
	        <div class="input-field col s12">
	          <input required name="add" id="add_reg" type="text" class="validate">
	          <label for="add_reg">ĐỊA CHỈ</label>
	        </div>

	    </div>

	    <div class="row">

		    <div class="input-field col s6">
	          <input required name="pass" id="password_reg" type="password" class="validate">
	          <label for="password_reg">Password</label>
	        </div>

	        <div class="input-field col s6">
	          <input required name="confpass" id="con_password_reg" type="password" class="validate">
	          <label for="con_password_reg">Confirm Password</label>
	        </div>
	        
		  </div>

		  <!-- <a href="javascript:void(0)" id="submit_reg" class="waves-effect waves-light btn" style="background: #ee6e73 !important;">Register</a> -->
	      <input type="submit" name="register" value="Register" >	
	    
	    </div>
	  </div>
    </form>
  </section>