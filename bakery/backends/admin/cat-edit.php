<?php

session_start();
try {

    if (!file_exists('../connection-pdo.php' ))
        throw new Exception();
    else
        require_once('../connection-pdo.php' ); 
		
} catch (Exception $e) {

	$_SESSION['msg'] = 'There were some problem in the Server! Try after some time!';

	header('location: ../../admin/category-list.php');

	exit();
	
}

if (!isset($_POST['loaisp_id']) || !isset($_POST['mota_ngan']) || !isset($_POST['mota_dai'])) {

	$_SESSION['msg'] = 'Invalid ID!';

	header('location: ../../admin/category-list.php');

	exit();
} 

	$loaisp_id = $_POST['loaisp_id'];
    $mota_ngan = $_POST['mota_ngan'];
    $mota_dai = $_POST['mota_dai'];


	$sql = "UPDATE 'loaisp' SET NAME loaisp_id='$loaisp_id' , mota_ngan='$mota_ngan' , mota_dai='$mota_dai' WHERE loaisp_id = $loaisp_id";
    $query  = $pdoconn->query($sql);
    if ($query) {

    	$_SESSION['msg'] = 'Category Edited!';

		header('location: ../../admin/category-list.php');
    	
    } else {

    	$_SESSION['msg'] = 'There were some problem in the server! Please try again after some time!';

		header('location: ../../admin/category-list.php');

    }

