-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2021 at 06:19 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bakery`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `pass`, `email`) VALUES
(0, 'admin', '1234', 'admin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_sp` int(11) NOT NULL,
  `soluong` int(11) NOT NULL,
  `hoadon` tinyint(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `donhang`
--

CREATE TABLE `donhang` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(50) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_food` int(11) NOT NULL,
  `user_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donhang`
--

INSERT INTO `donhang` (`id`, `name`, `status`, `id_user`, `id_order`, `id_food`, `user_name`, `timestamp`) VALUES
(1, 'bánh bông lan', 1, 1, 1, 2, 'Tien phan', '1/11/2021');

-- --------------------------------------------------------

--
-- Table structure for table `inf_user`
--

CREATE TABLE `inf_user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdt` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inf_user`
--

INSERT INTO `inf_user` (`id`, `name`, `email`, `pass`, `sdt`, `address`) VALUES
(1, 'TIENPHAN', 'TIEN@GMAIL.COM', '123', 392355239, 'PHÚ YÊN'),
(2, 'na', 'na@gmail.com', '123', 12334, 'nha trang'),
(3, 'duyen', 'duyen@GMAIL.COM', '123', 12345, ''),
(5, 'doan', 'tien.ptc.60cntt@ntu.edu.vn', '123', 392355239, 'nha trang'),
(6, 'anna', 'tien.ptc.60cntt@ntu.edu.vn', '123', 2147483647, 'Phú Yên');

-- --------------------------------------------------------

--
-- Table structure for table `loaisp`
--

CREATE TABLE `loaisp` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mota_ngan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mota_dai` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loaisp`
--

INSERT INTO `loaisp` (`id`, `name`, `mota_ngan`, `mota_dai`, `image`) VALUES
(4, 'RAU CÂU', 'cùng thưởng thức món thạch rau câu cùng mời cả nhà và người thương thì còn gì tuyệt vời hơn', 'Được làm hoàn toàn thủ công với nhiều loại nguyên liệu đa dạng', 'rc2.jpg'),
(6, 'CAKE_EATCLEAR', 'Từ các sản phẩm yến mạch, ngũ cốc, mang lại sự mới mẻ', 'Bánh được làm từ các loại hạt: hạnh nhân, hạt điều, hạt bí xanh, óc chó, nam việt quất, mè trắng, mè đen ạ và tạo ngọt bằng một ít mật ong cho dễ ăn thôi ạ, nên bánh ăn ngọt rất nhẹ nha. ', '98.jpg'),
(11, 'BÁNH NGỌT', 'Bánh được làm và nướng nóng sau khi khách đặt, tươi mới mỗi ngày', 'Với nhiều mẫu mã đa dạng, theo xu hướng giới trẻ, nhưng không quên nét cổ điển.', 'banhngot.jpg'),
(12, 'BÁNH COOKIE', 'Những chiếc cookie này dùng để làm quà và tặng rất phù hợp đấy nhé !', 'Một trong những chiếc cookie cổ điển nhưng không kém phần trang trọng ( một loại cookie giòn mỏng có vị gừng). Công thức cho chiếc cookie này sử dụng một lượng lớn mật đường để đảm bảo độ mềm và ẩm của bánh trong vòng 2 tuần.', 'cook.jpg'),
(14, 'BÁNH KEM', 'Với đa dạng các loại bánh , được trang trí theo nhu cầu', 'Đặt ngay tại cửa hàng từ 1-5h để nhận được sản phẩm.Bánh được thiết kế dạng bánh mini dễ dùng, phù hợp với nhiều loại tiệc như khai trương, sinh nhật và đặc biệt là các tiệc thành hôn, đám hỏi,...\r\n\r\n', 'b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sanpham`
--

CREATE TABLE `sanpham` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(255) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mota` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_loai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sanpham`
--

INSERT INTO `sanpham` (`id`, `name`, `price`, `status`, `mota`, `image`, `id_loai`) VALUES
(11, 'RAU CÂU LÁ CẨM', 50, 'Mỗi hộp 200gram', '\r\nNhững miếng rau câu màu sắc sặc sỡ được làm từ nguyên liệu lá cẩm tự nhiên đảm bảo cho sức khỏe. \r\n\r\nTrọng lượng: 1 set rau câu lá cẩm gồm 15 cái\r\nThành phần: lá cẩm, cốt dừa,...\r\nRau câu được chuẩn bị sau khi khách đặt, đảm bảo chất lượng luôn mới và thơm ngon.', 'RC.JPG', 4),
(13, 'RAU CÂU THẬP CẨM\r\n', 100, 'Mỗi hộp 200gram', 'Những miếng rau câu không chỉ thơm ngon mà còn thật bắt mắt bởi màu sắc sặc sỡ được làm từ nguyên liệu tự nhiên đảm bảo cho sức khỏe. Đây không chỉ làm kích thích vị giác mà còn là điểm nhấn cho bữa tiệc liên hoan hoặc ngay cả những sự kiện hội họp tại văn phòng.\r\n\r\nTrọng lượng: 1 set rau câu lthập cẩm gồm 15 cái\r\nRau câu được chuẩn bị sau khi khách đặt, đảm bảo chất lượng luôn mới và thơm ngon.', 'rc2.jpg', 4),
(14, 'RAU CÂU LONG NHÃN\r\n', 50, 'Mỗi hộp 200gram', '\r\nRau câu thanh mát, kèm theo long nhãn thơm ngọt là combo giải nhiệt tuyệt hảo nhất cho mùa hè. Bạn có thể chọn món rau câu này cho ngày sinh nhật, họp mặt gia đình hoặc sự kiện khai trương,...\r\n\r\nTrọng lượng: 1 set rau câu Long Nhãn gồm 15 cái\r\nThành phần: long nhãn, cốt dừa,...\r\nRau câu được chuẩn bị sau khi khách đặt, đảm bảo chất lượng luôn mới và thơm ngon.', 'rc3.jpg', 4),
(20, 'CREP_SOCOLA', 50, 'Khuyến mãi 5%', 'Được làm từ bột mì 50G', '5.jpg', 12),
(22, 'BÁNH KEM TƯƠI', 200, 'Khuyến mãi 15%', 'Thành phần chính:\r\n\r\n- Gato,\r\n\r\n- Kem tươi trà xanh ,  vị rượu rum,\r\n\r\n-Bột Trà xanh.\r\n\r\nBánh làm từ 3 lớp gato trắng xen giữa 3 lớp kem tươi trà xanh  vị rượu rum (nho). Bên ngoài bánh phủ 1 LỚP BỘT TRÀ XANH VÀ TRANG TRÍ HOA QUẢ.', 'kemtuoi.jpg', 14),
(23, 'RED VELVET CAKE', 300, 'Khuyến mãi 5%', 'Thành phần chính:\r\n\r\n- Gato,\r\n\r\n- Bột mỳ đỏ,\r\n\r\n- Kem tiramisu.\r\n\r\nBánh làm từ 3 lớp gato đỏ xen lẫn 3 lớp kem tươi. Bên trên bánh phủ 1 lớp kem tiramisu rắc bột mỳ đỏ.', 'red.jpg', 14),
(24, 'CAPUCCINO', 250, 'Khuyến mãi 5%', 'Thành phần chính:\r\n\r\n- Gato\r\n\r\n- Kem phomai vị coffee\r\n\r\n- Cacao.\r\n\r\nBánh làm từ 3 lớp gato TRẮNG xen giữa 3 lớp kem TƯƠI PHOMAI, VỊ COFFEE. Bên ngoài phủ 1 lớp bột cacao VÀ DECOR HOA QUẢ. ', 'capu.jpg', 14),
(25, 'CARAMEL MOIST ', 200, 'Khuyến mãi 5%', 'Thành phần chính:\r\n\r\n- Gato\r\n\r\n- Sốt caramel\r\n\r\n- Kem tươi\r\n\r\nBánh làm từ 3 lớp gato socola xen giữa 3 lớp kem tươi vị socola. Phủ bên ngoài là 1 lớp sốt caramel có vị đắng nhẹ.', 'cam.jpg', 14),
(26, 'DELI CAKE', 150, 'khuyến mãi 10%', 'Thành phần chính:\r\n\r\n- Gato\r\n\r\n- Kem phomai\r\n\r\n- Hoa quả\r\n\r\n- Socola.\r\n\r\nBánh làm từ 3 lớp gato xen giữa 3 lớp kem. Bánh phủ bên ngoài bởi 1 lớp kem phomai, phía trên được trang trí bằng hoa quả và socola bao quanh.', 'cake.jpg', 14),
(27, 'MUFFIN SOCOLA', 15, '6 cái/hộp', 'Thành phần:\r\n\r\n- Gato vị socola\r\n\r\n- Hạnh nhân cắt lát', 'mu.jpg', 12),
(28, 'BÁNH MÌ HOA CÚC', 30, 'Khuyến Mãi 3%', 'Sự kết hợp tinh tế giữ hoa cúc xấy khô và gato mềm mịn. Kích thích vị giác.', 'cuc.jpg', 11),
(29, 'BÁNH LƯỠI MÈO', 50, 'Gói 500gram', 'Thành phần chính: \r\n\r\n- Bột mỳ,\r\n\r\n- Kem bơ,\r\n\r\n- Đường,\r\n\r\n- Lòng trắng trứng.', 'meo.jpg', 12),
(31, 'COCONUT - DỪA', 30, 'Goi 300gram', 'Thành phần: \r\n\r\n- Đường,\r\n\r\n- Lòng trắng trứng,\r\n\r\n- Dừa nạo.\r\n\r\n', 'dua.jpg', 12),
(32, 'RAISIN NHO28', 28, 'Gói 300gram', 'Thành phần chính:\r\n\r\n- Bột mỳ,\r\n\r\n- Đường,\r\n\r\n- Bơ,\r\n\r\n- Trứng,\r\n\r\n- Nho khô.', 'nho.jpg', 12),
(33, 'PALMIER', 30, 'Gói 200gram', 'Thành phần:\r\n\r\n- Bột mì,\r\n\r\n- Đường,\r\n\r\n- Bơ,\r\n\r\n- Muối.', 'pa.jpg', 12),
(34, 'RED VELVET COOKIES', 50, 'Gói 200gram', 'Thành phần:\r\n\r\n- Bột mỳ đỏ,\r\n\r\n- Bơ,\r\n\r\n- Socola,\r\n\r\n- Trứng.', 'hat.jpg', 12),
(35, 'CIGARETTES', 50, 'Gói 300gram', 'Thành phần: \r\n\r\n- Bột mỳ,\r\n\r\n- Đường,\r\n\r\n- Lòng trắng trứng,\r\n\r\n- Bơ.', 'ong.jpg', 12),
(36, 'CLASSIC FRENCH', 60, 'Số lượng: 4 cái/hộp', 'Thành phần: \r\n\r\n- Bột mỳ\r\n\r\n- Bơ\r\n\r\n- Đường\r\n\r\n- Kem\r\n\r\n- Dầu\r\n\r\n- Nho', 'nhoo.jpg', 11),
(37, 'CHOCOLATE DONUT', 60, ' Số lượng: 4 cái/hộp', 'Thành phần: \r\n\r\n- Bột mỳ\r\n\r\n- Dầu ăn\r\n\r\n- Bơ, sữa\r\n\r\n- Socola', 'donut.jpg', 11),
(38, 'BLUEBERRY PUFF PASTRY ', 80, ' Số lượng: 4 cái/hộp', 'Thành phần chính: \r\n\r\n- Bột mỳ\r\n\r\n- Bơ\r\n\r\n- Mứt Việt Quất\r\n\r\n- Phụ gia', 'berry.jpg', 11),
(39, 'PAIN CHOCOLATE', 50, ' Số lượng: 3 cái/hộp', 'Thành phần chính: \r\n\r\n- Bột mỳ\r\n\r\n- Bơ\r\n\r\n- Socola\r\n\r\n- Phụ gia', 'pain.jpg', 11),
(40, 'BISCOTTI RED BRICK ', 100, 'Gói 400gram', 'Với thành phần nguyên liệu tự nhiên, chuẩn organic, được làm chủ yếu từ trái cây và hạt dinh dưỡng.\r\nThành phần chính: bột mì nguyên cám, lòng trắng trứng, óc chó, hạnh nhân, hạt bí, hướng dương,… \r\nGiúp bổ sung chất xơ, protein, vitamin và khoáng chất', 'ngu.jpg', 6),
(41, 'BISCOTTI VỊ MATCHA', 80, 'Gói 300gram', '- Biscotti ăn kiêng là loại bánh quy được nướng hai lần, miếng bánh mỏng giòn nhẹ, tạo cảm giác thích thú khi thưởng thức\r\n- Nguyên liệu bao gồm bột nguyên cám và các loại hạt dinh dưỡng( hạt nhân, hạt điều, nho,...)\r\n- Sản phẩm có 3 vị chính: chocolate, matcha, vani.\r\n- Thơm - xốp - mềm là điểm đặc trưng của món bánh biscotti', 'bis.jpg', 6),
(42, 'BÁNH NGŨ CỐC', 50, 'Gói 200gram', 'Bánh được làm hoàn toàn từ các loại hạt hạnh nhân, điều, óc chó, bí xanh, nam việt quất. Cam kết không đường, không bột, rất thích hợp với các bạn đang eat clean.', 'coc.jpg', 6),
(43, 'RAU CÂU DỪA-CACAO', 30, 'Mỗi hộp 200gm', 'Nguyên liệu chính\r\n -Nước dừa tươi: 4 trái\r\n -Bột rau câu dẻo: 10 gram\r\n -Đường: 200 gram\r\n -Sữa tươi: 100ml\r\n -Nước cốt dừa lon: 3 thìa canh', 'cacao.jpg', 4),
(44, 'RAU CÂU SỢI GIÒN', 50, 'Mỗi hộp 200gram', 'Nguyên liệu chính:\r\n -Rau câu con cá Thái 25 gr\r\n -Nước cốt dừa 300 gr\r\n -Sữa đặc 200 gr\r\n -Cà phê hòa tan 3 gr\r\n -Nước cốt lá dứa 4 muỗng canh\r\n -Rau câu sợi 30 gr\r\n -Đường 200 gr\r\n -Nước 2 lít Vani 1 ít\r\n -Muối 1 gr', 'soi.jpg', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donhang`
--
ALTER TABLE `donhang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inf_user`
--
ALTER TABLE `inf_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loaisp`
--
ALTER TABLE `loaisp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`id`),
  ADD KEY `_FK_loasp` (`id_loai`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `donhang`
--
ALTER TABLE `donhang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `inf_user`
--
ALTER TABLE `inf_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `loaisp`
--
ALTER TABLE `loaisp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `sanpham`
--
ALTER TABLE `sanpham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD CONSTRAINT `_FK_loasp` FOREIGN KEY (`id_loai`) REFERENCES `loaisp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
