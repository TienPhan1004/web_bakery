<?php require('layout/header.php'); ?>
<?php require('layout/left-sidebar-long.php'); ?>
<?php require('layout/topnav.php'); ?>
<?php require('layout/left-sidebar-short.php'); ?>


<?php

require('../backends/connection-pdo.php');

$sql = 'SELECT * FROM donhang LEFT JOIN sanpham ON donhang.id = sanpham.id';

$query  = $pdoconn->query($sql);
$num=$query->num_rows;
$arr_all=$query->fetch_assoc();



?>
						

<div class="section white-text" style="background: #B35458;">

	<div class="section">
		<h3>Orders</h3>
	</div>

  <?php

    if (isset($_SESSION['msg'])) {
        echo '<div class="section center" style="margin: 5px 35px;"><div class="row" style="background: red; color: white;">
        <div class="col s12">
            <h6>'.$_SESSION['msg'].'</h6>
            </div>
        </div></div>';
        unset($_SESSION['msg']);
    }

    ?>
	
	<div class="section center" style="padding: 20px;">
		<table class="centered responsive-table">
        <thead>
          <tr>
              <th>Order ID</th>
              <th>User Name</th>
              <th>Food Name</th>
              <th>Timestamp</th>
              <th>Tổng Bill</th>
          </tr>
        </thead>

        <tbody>
          <?php

            while( $arr_all=$query->fetch_assoc()) {

          ?>
          <tr>
            <td><?php echo $arr_all['id']; ?></td>
            <td><?php echo $arr_all['user_name']; ?></td>
            <td><?php echo $arr_all['name']; ?></td>
            <td><?php echo $arr_all['timestamp']; ?></td>
            <td><?php echo $arr_all['hoadon']; ?></td>
          </tr>

          <?php } ?>
         
        </tbody>
      </table>
	</div>
</div>

<?php require('layout/about-modal.php'); ?>
<?php require('layout/footer.php'); ?>