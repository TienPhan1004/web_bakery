<?php require('layout/header.php'); ?>
<?php require('layout/left-sidebar-long.php'); ?>
<?php require('layout/topnav.php'); ?>
<?php require('layout/left-sidebar-short.php'); ?>


<?php

require('../backends/connection-pdo.php');
if(isset($_GET['id'])){
    $sanpham_id = $_GET['id'];
    $sql_sp = "SELECT * FROM sanpham WHERE id='$sanpham_id'";
    $query_sp = $pdoconn->query($sql_sp);

    if($query_sp->num_rows >0){
        $selected="selected";
        $arr_all_sp=$query_sp->fetch_assoc();

        $sql_loai = "SELECT * FROM loaisp";
        $query_loai = $pdoconn->query($sql_loai);
        $num_loai = $query_loai->num_rows;
        // $arr_all_loai=$query_loai->fetch_assoc();

?>

<!--<script> $("input[name='sanpham_id']").hide(); </script>-->
<div class="section white-text" style="background: #B35458;">

	<div class="section">
		<h3>Sửa sản phẩm</h3>
	</div>


    <div class="section center" style="padding: 40px;">

        <form action="../backends/admin/food-edit.php" method="POST">
            <input type="hidden" name="sanpham_id" value="<?php echo $sanpham_id; ?>"/>
            <?php

            if (isset($_SESSION['msg'])) {
                echo '<div class="row" style="background: red; color: white;">
                <div class="col s12">
                    <h6>'.$_SESSION['msg'].'</h6>
                    </div>
                </div>';
                unset($_SESSION['msg']);
            }

            ?>

            <div class="row">
                <div class="col s6" style="">
                            <div class="input-field">
                            <input value="<?php echo $arr_all_sp['name']?>" id="name" name="name" type="text" class="validate" style="color: white; width: 70%">
                            <label for="name" style="color: white;"><b>Food Name :</b></label>
                            </div>
                </div>
                <div class="col s6" style="">
                            <div class="input-field" style=" width: 90%">
						    <select name='loai'>
						      <?php 

						      		while($arr_all_loai=$query_loai->fetch_array()) {
                                        if($arr_all_loai['id'] != $arr_all_sp['id_loai']) 
                                            echo '<option value="'.$arr_all_loai['id'].'" selected>'.$arr_all_loai['name'].'</option>';
                                        else
						      			    echo '<option value="'.$arr_all_loai['id'].'" ></option>'.$arr_all_loai['name'].'</option>';
						      		}
						      ?>
						    </select>
						    <label style="color: white;">Categories</label>
						  </div>
                </div>
            </div>

            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input value="<?php echo $arr_all_sp['mota']?>" id="desc" name="mota" type="text" class="validate" style="color: white; width: 70%">
                        <label for="desc" style="color: white;"><b>Description :</b></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input value="<?php echo $arr_all_sp['status']?>" id="desc" name="status" type="text" class="validate" style="color: white; width: 70%">
                        <label for="desc" style="color: white;"><b>Status :</b></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input value="<?php echo $arr_all_sp['price']?>" id="desc" name="price" type="number" class="validate" style="color: white; width: 70%">
                        <label for="desc" style="color: white;"><b>Giá :</b></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input value="<?php echo $arr_all_sp['image']?>" id="desc" name="images" type="file" class="validate" style="color: white; width: 70%">
                        <label for="desc" style="color: white;"><b>Images :</b></label>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col s12">
                    <div class="section right" style="padding: 15px 10px;">
                        <a href="food-list.php" class="waves-effect waves-light btn">Dismiss</a>
                    </div>
                    <div class="section right" style="padding: 15px 20px;">
                        <button type="submit" class="waves-effect waves-light btn">Save edit</button>
                    </div>
                </div>
            </div>

        </form>
    </div>

</div>
<?php }} ?>
<?php require('layout/about-modal.php'); ?>
<?php require('layout/footer.php'); ?>