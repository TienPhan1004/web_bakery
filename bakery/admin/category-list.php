<?php require('layout/header.php'); ?>
<?php require('layout/left-sidebar-long.php'); ?>
<?php require('layout/topnav.php'); ?>
<?php require('layout/left-sidebar-short.php'); ?>


<?php

require('../backends/connection-pdo.php');

$sql = 'SELECT * FROM loaisp';

$query  = $pdoconn->query($sql);


?>
						

<div class="section white-text" style="background: #B35458;">

	<div class="section">
		<h3>Categories</h3>
	</div>

  <?php

    if (isset($_SESSION['msg'])) {
        echo '<div class="section center" style="margin: 5px 35px;"><div class="row" style="background: red; color: white;">
        <div class="col s12">
            <h6>'.$_SESSION['msg'].'</h6>
            </div>
        </div></div>';
        unset($_SESSION['msg']);
    }

    ?>

	<div class="section right" style="padding: 15px 25px;">
		<a href="category-add.php" class="waves-effect waves-light btn">Add New</a>
	</div>
	
	<div class="section center" style="padding: 20px;">
		<table class="centered responsive-table">
        <thead>
          <tr>
              <th>Name</th>
              <th>Short Description</th>
              <th>Action</th>
          </tr>
        </thead>

        <tbody>
          <?php
            if($query->num_rows !=0){
              while($arr_all=$query->fetch_array())  {

                ?>
                <tr>
                  <td><?php echo $arr_all['name']; ?></td>
                  <td><?php echo $arr_all['mota_ngan']; ?></td>
                  <td>
                    <a href="../backends/admin/cat-delete.php?id=<?php echo $arr_all['id']; ?>"><span class="new badge" data-badge-caption="">Delete</span></a>
                    <a href="./category-edit.php?id=<?php echo $arr_all['id']; ?>"><span class="new badge" data-badge-caption="">Edit</span></a>
                
                  </td>
      
                </tr>
      
                <?php }} ?>
            
            
         
        </tbody>
      </table>
	</div>
</div>

<?php require('layout/about-modal.php'); ?>
<?php require('layout/footer.php'); ?>