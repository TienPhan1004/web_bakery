<?php require('layout/header.php'); ?>
<?php require('layout/left-sidebar-long.php'); ?>
<?php require('layout/topnav.php'); ?>
<?php require('layout/left-sidebar-short.php'); ?>

<?php

require('../backends/connection-pdo.php');
if(isset($_GET['id'])){
    $loaisp_id = $_GET['id'];
    $sql_loaisp = "SELECT * FROM `loaisp` WHERE id={$loaisp_id}";
    $query_loaisp = $pdoconn->query($sql_loaisp);

    if($query_loaisp->num_rows >0){
        $arr_all_loaisp=$query_loaisp->fetch_assoc();

?>

<div class="section white-text" style="background: #B35458;">

	<div class="section">
		<h3>Edit Categories</h3>
	</div>


    <div class="section center" style="padding: 40px;">

        <form action="../backends/admin/cat-add.php" method="post">
        <input type="hidden" name="loaisp_id" value="<?php echo $loaisp_id; ?>"/>
            <?php

            if (isset($_SESSION['msg'])) {
                echo '<div class="row" style="background: red; color: white;">
                <div class="col s12">
                    <h6>'.$_SESSION['msg'].'</h6>
                    </div>
                </div>';
                unset($_SESSION['msg']);
            }

            ?>

            <div class="row">
                <div class="col s6" style="">
                            <div class="input-field">
                            <input value="<?php echo $arr_all_loaisp['name']?>" id="name" name="name" type="text" class="validate" style="color: white; width: 70%">
                            <label for="name" style="color: white;"><b>Category Name :</b></label>
                            </div>
                </div>
               
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input value="<?php echo $arr_all_loaisp['mota_ngan']?>" id="short_desc" name="mota_ngan" type="text" class="validate" style="color: white; width: 70%">
                        <label for="long_desc" style="color: white;"><b>Short Description :</b></label>
                    </div>
                </div>
            
            </div>

            <div class="row">
                <div class="col s12">
                    <div class="input-field">
                        <input value="<?php echo $arr_all_loaisp['mota_dai']?>" id="long_desc" name="mota_dai" type="text" class="validate" style="color: white; width: 70%">
                        <label for="long_desc" style="color: white;"><b>Long Description :</b></label>
                    </div>
                </div>
            
            </div>

            <div class="row">
                <div class="col s12">
                    <div class="section right" style="padding: 15px 10px;">
                        <a href="category-list.php" class="waves-effect waves-light btn">Dismiss</a>
                    </div>
                    <div class="section right" style="padding: 15px 20px;">
                        <button type="submit" class="waves-effect waves-light btn">Save edit</button>
                    </div>
                </div>
            </div>

        </form>


    </div>

</div>
<?php }} ?>
<?php require('layout/about-modal.php'); ?>
<?php require('layout/footer.php'); ?>