<?php require('layout/header.php'); ?>
<?php require('layout/left-sidebar-long.php'); ?>
<?php require('layout/topnav.php'); ?>
<?php require('layout/left-sidebar-short.php'); ?>


<?php

require('../backends/connection-pdo.php');
if(!isset($_GET['page'])){
  $_GET['page'] = 1;
}
$rowPerPage = 6;
// vị trí của mẩu tin đầu tiên trên mỗi trang
$offset = ($_GET['page'] - 1) * $rowPerPage;

$sql = "SELECT * FROM sanpham LIMIT $offset, $rowPerPage";  
$query  = $pdoconn->query($sql);
$num=$query->num_rows;
$arr_all=$query->fetch_assoc();


?>
						

<div class="section white-text" style="background: #B35458;">

	<div class="section">
		<h3>Foods</h3>
	</div>

  <?php

    if (isset($_SESSION['msg'])) {
        echo '<div class="section center" style="margin: 5px 35px;"><div class="row" style="background: red; color: white;">
        <div class="col s12">
            <h6>'.$_SESSION['msg'].'</h6>
            </div>
        </div></div>';
        unset($_SESSION['msg']);
    }

    ?>

	<div class="section right" style="padding: 15px 25px;">
		<a href="food-add.php" class="waves-effect waves-light btn">Add New</a>
	</div>
	
	<div class="section center" style="padding: 20px;">
		<table class="centered responsive-table">
        <thead>
          <tr>
              <th>STT</th>
              <th>Name</th>
              
              <th>Image</th>
              <th>price</th>
              <th>Status</th>
              <th>Action</th>
          </tr>
        </thead>

        <tbody>
          <?php
          $tam =1;

          while ($arr_all=$query->fetch_assoc()) {
             
          ?>
          <tr>
            <td>
              <?php
              echo $tam;
              ?>
            </td>
            <td><?php echo $arr_all['name']; ?></td>
            <td> <img width="50px" src="../images/<?php echo $arr_all['image']; ?>"></td>
            <td><?php echo $arr_all['price']; ?></td>
            <td><?php echo $arr_all['status']; ?></td>
            <td>
              <a href="../backends/admin/food-delete.php?id=<?php echo $arr_all['id']; ?>"><span class="new badge" data-badge-caption="">Delete</span></a>
              <a href="./food-edit.php?id=<?php echo $arr_all['id']; ?>"><span class="new badge" data-badge-caption="">Edit</span></a>
            </td>
          </tr>

          <?php $tam++; } ?>
         
        </tbody>
      </table>
	</div>
</div>
<?php
        $re = $pdoconn->query("SELECT * FROM sanpham ");
        $numRows = mysqli_num_rows($re);
        $maxPage = ceil($numRows/$rowPerPage);
?>
<center>
<nav align="center" class="d-flex justify-content-end p-4" aria-label="Page navigation example">
        <ul class="pagination">
        <li class="page-item">
                <?php
                    if($_GET["page"] > 1){ ?>
                        <a class="page-link" href="food-list.php?page=<?= $_GET['page'] - 1 ?>" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                <?php } ?>
                </li>
            <?php 
                for ($i=1 ; $i<=$maxPage ; $i++)
                {
                    if($i == $_GET['page']){ ?>
                        <li class="page-item">
                            <a class="page-link active" href="food-list.php?page=<?= $i ?>"><?= $i ?></a> <!-- trang hiện tại sẽ được style -->
                        </li>
                    <?php }
                    else { ?>
                        <li class="page-item">
                            <a class="page-link" href="food-list.php?page=<?= $i ?>"><?= $i ?></a> 
                        </li>
                    <?php }
                }
            ?>
            <li class="page-item">
                <?php
                    if($_GET["page"] < $maxPage){ ?>
                        <a class="page-link" href="food-list.php?page=<?= $_GET['page'] + 1 ?>" aria-label="Previous">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    <?php }
                    
                    $pdoconn->close();
                ?>
            </li>
        </ul>
    </nav>
    </center>
<?php require('layout/about-modal.php'); ?>
<?php require('layout/footer.php'); ?>